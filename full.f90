!-----------------------------------------------------------------------
program full
  !-----------------------------------------------------------------------
  !
  ! read in PWSCF data in XML format using IOTK lib
  ! then prepare matrices for GWL calculation
  ! 
  ! input:  namelist "&inputpp", with variables
  !   prefix       prefix of input files saved by program pwscf
  !   outdir       temporary directory where files resides
  !   pp_file      output file. If it is omitted, a directory 
  !                "prefix.export/" is created in outdir and
  !                some output files are put there. Anyway all the data 
  !                are accessible through the "prefix.export/index.xml" file which
  !                contains implicit pointers to all the other files in the
  !                export directory. If reading is done by the IOTK library
  !                all data appear to be in index.xml even if physically it
  !                is not. 
  !   uspp_spsi    using US PP if set .TRUE. writes S | psi > 
  !                and | psi > separately in the output file 
  !   single_file  one-file output is produced
  !   ascii        ....
  !
  !   pseudo_dir   pseudopotential directory
  !   psfile(:)    name of the pp file for each species 
  !    

  use io_files,  ONLY : prefix, tmp_dir
  use io_files,  ONLY : psfile, pseudo_dir
  use io_global, ONLY : stdout, ionode, ionode_id
  USE mp_global, ONLY: mp_startup
  USE mp_pools, ONLY : kunit
  use mp_world, ONLY: mpime, world_comm 
  USE environment,   ONLY: environment_start
  USE mp, ONLY : mp_bcast
  use ldaU, ONLY : lda_plus_u
  use scf, only : vrs, vltot, v, kedtau
  USE fft_base,             ONLY : dfftp
  use uspp, ONLY : okvan
  use realus,  ONLY : generate_qpointlist
  USE io_files, ONLY : seqopn
  USE wannier_gw, ONLY : l_truncated_coulomb, truncation_radius,num_nbndv,vg_q,numw_prod,&
                           &num_nbnds,s_first_state,s_last_state
  USE gvect, ONLY : ngm
  USE gvecs,                ONLY : doublegrid
  USE lsda_mod,      ONLY : nspin

  implicit none
  character(len=9) :: code = 'FULL'
  integer :: ios, kunittmp
  CHARACTER(LEN=256), EXTERNAL :: trimcheck
  character(len=200) :: pp_file
  logical :: uspp_spsi, ascii, single_file, raw
  logical :: exst
  INTEGER, external :: find_free_unit
  CHARACTER(len=256) :: prefix_nc 
  INTEGER :: nglobals
  LOGICAL :: l_dipols
  CHARACTER(LEN=256) :: outdir

  NAMELIST /inputfull/ prefix,l_truncated_coulomb,truncation_radius,num_nbndv,numw_prod,&
                        &num_nbnds,nglobals,s_first_state,s_last_state,prefix_nc,l_dipols

  CALL mp_startup ( )
  CALL environment_start ( code )

  prefix='export'
  CALL get_environment_variable( 'ESPRESSO_TMPDIR', outdir )
  IF ( TRIM( outdir ) == ' ' ) outdir = './'
  IF ( ionode ) THEN
     CALL input_from_file ( )
     READ(5,inputfull,IOSTAT=ios)
     IF (ios /= 0) CALL errore ('FULL', 'reading inputfull namelist', ABS(ios) )
  endif

  tmp_dir = trimcheck( outdir )
  CALL mp_bcast( outdir, ionode_id, world_comm  )
  CALL mp_bcast( tmp_dir, ionode_id , world_comm )
  CALL mp_bcast( prefix, ionode_id , world_comm )
  CALL mp_bcast( l_truncated_coulomb, ionode_id , world_comm )
  CALL mp_bcast( truncation_radius, ionode_id , world_comm )
  CALL mp_bcast( num_nbndv, ionode_id , world_comm )
  CALL mp_bcast(numw_prod, ionode_id, world_comm )
  CALL mp_bcast(num_nbnds, ionode_id, world_comm )
  CALL mp_bcast(nglobals, ionode_id, world_comm )
  CALL mp_bcast(s_first_state, ionode_id, world_comm )
  CALL mp_bcast(s_last_state, ionode_id, world_comm )
  CALL mp_bcast(prefix_nc, ionode_id, world_comm )
  CALL mp_bcast(l_dipols, ionode_id, world_comm )

  if(.not.l_truncated_coulomb) numw_prod=numw_prod+1

  call read_file

  call openfile_school

#if defined __PARA
  kunittmp = kunit
#else
  kunittmp = 1
#endif

  pp_file= ' '
  uspp_spsi = .FALSE.
  ascii = .FALSE.
  single_file = .FALSE.
  raw = .FALSE.


  call read_export(pp_file,kunittmp,uspp_spsi, ascii, single_file, raw)


  call summary()
  
  CALL print_ks_energies()

  CALL hinit0()
!                                                                                               
  if(lda_plus_u) then
    CALL init_ns()
  endif
  CALL set_vrs(vrs, vltot, v%of_r, kedtau, v%kin_r, dfftp%nnr, nspin, doublegrid )

  IF ( okvan) CALL generate_qpointlist()
 

  

  allocate(vg_q(ngm))
  if(.not.l_truncated_coulomb) then
     call calculate_vg0
  endif

  
 
  call calculate_vg0_ir    
  call exchange_full_ir_k


  deallocate(vg_q)
  call stop_pp

  stop
end program full



