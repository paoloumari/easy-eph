# Makefile for  full

include ../../make.inc
MODFLAGS= $(BASEMOD_FLAGS) \
          $(MOD_FLAG)../../PW/src 


FULLOBJS = \
  stop_pp.o \
  read_export.o \
  openfile_full.o \
  exchange_full_ir_k.o \

QEMODS = ../../Modules/libqemod.a ../../FFTXlib/libqefft.a \
	../../upflib/libupf.a  ../../KS_Solvers/libks_solvers.a \
	../../LAXlib/libqela.a ../../UtilXlib/libutil.a \
	../../dft-d3/libdftd3qe.a ../../XClib/xc_lib.a

           PWOBJS = ../../PW/src/libpw.a 
           PHOBJS = ../../PHonon/Gamma/libphcg.a 

GWWOBJ = ../gww/libgww.a
PW4GWWOBJS  = ../pw4gww/wannier_uterms.o
TLDEPS= pwlibs gwwlib


all : tldeps full.x

full.x : full.o libfull.a  $(FULLOBJS) $(PWOBJS) $(QEMODS) $(PW4GWWOBJS)  
	$(LD) $(LDFLAGS) -o $@ \
		full.o libfull.a $(PWOBJS) $(PW4GWWOBJS) $(QEMODS) $(LIBOBJS) $(QELIBS) 
	- ( cd ../../bin ; ln -fs ../FULL/$@ . )

tldeps :
	if test -n "$(TLDEPS)" ; then   ( cd ../.. ; $(MAKE) $(TLDEPS) || exit 1 ) ; fi

libfull.a : $(FULLOBJS)
	$(AR) $(ARFLAGS) $@ $?
	$(RANLIB) $@

clean :
	- /bin/rm -f *.x *.o *~ *.F90 *.d *.mod *.i *.L libfull.a

include make.depend
# DO NOT DELETE

