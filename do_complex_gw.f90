program do_complex_gw


  implicit none
  
  integer :: nat
  integer :: nskip
  integer :: nmodx
  real(kind=8),allocatable :: freq(:),imepsi(:),reepsi(:),oscillator(:) 
  real(kind=8) :: eta
  real(kind=8) :: eps_inf
  real(kind=8) :: e_max
  integer :: nn,i,in,j,nexc
  real(kind=8) :: ene
  complex(kind=8) :: csca,enne_cmplx
  real(kind=8) :: w_av_e, eta_e,w_p_e,w_pl_e
  real(kind=8) :: exc_min,exc_max
  complex(kind=8), allocatable :: eff(:),eff2(:),eff_wo(:)
  real(kind=8),allocatable :: exc(:),integ(:)
  integer :: idumm1,idumm2
  real(kind=8) :: rdumm,omega,enne,kappa,abs
  real(kind=8) :: eps0, delta_eps,offset
  real(kind=8) :: ef_mass,kmax,eps_eff,dk,eps_eff_loc
  real(kind=8), allocatable :: ene_values(:), f_values(:), k_values(:)

  INTEGER :: iband
  CHARACTER(4) :: nfile


  write(*,*) 'Volume a.u.'
  read(*,*) omega
  write(*,*) 'Number of atoms:'
  read(*,*) nat
  write(*,*) 'N skip'
  read(*,*) nskip
  write(*,*) 'Eta'
  read(*,*) eta
  write(*,*) 'Epsilon inf'
  read(*,*) eps_inf
  write(*,*) 'Electronic Transition energy w_av (ev) :'
  read(*,*) w_av_e
  write(*,*) 'Electronic broadening (ev) :'
  read(*,*) eta_e

   w_p_e=sqrt((eps_inf-1.d0)*w_av_e**2.d0)
   w_pl_e=sqrt(w_av_e**2.d0+w_p_e**2.d0)

   write(*,*) 'Plasmon frequency electronic (eV)',w_pl_e

  write(*,*) 'Energy max (eV)'
  read(*,*) e_max
  write(*,*) 'Number of steps'
  read(*,*) nn
  write(*,*) 'Number of  k values'
  read(*,*) nexc
  write(*,*) 'k max'
  read(*,*) kmax
  write(*,*) 'Effective mass'
  read(*,*) ef_mass
  write(*,*) 'Offset energy (eV)'
  read(*,*) offset
  write(*,*) 'Band label'
  read(*,*) iband


  nmodx=3*nat
  write(nfile,'(4i1)') iband/1000,mod(iband,1000)/100,mod(iband,100)/10,mod(iband,10)

  allocate(freq(nmodx),imepsi(nmodx),reepsi(nmodx),oscillator(nmodx)) 
  allocate(eff(nexc),exc(nexc),integ(nexc),eff2(nexc),eff_wo(nexc))
  allocate(ene_values(nexc),f_values(nexc),k_values(nexc))

  open(unit=20,file='infrared_generic.dat',status='old')
  do i=1,nmodx
     read(20,*) idumm1, freq(i),rdumm, reepsi(i)
     !read(20,*) freq(i),reepsi(i)     
  enddo
  close(20)
  freq=freq/(8065.6d0*2.d0*13.6058)
!  reepsi=reepsi*4.d0*3.1415926d0/omega/1822.89d0
  reepsi=reepsi/10508.9*3.1415926/omega/3.
  offset=offset/(2.d0*13.6058)
 ! reepsi=reepsi/omega/1822.89d0*(0.529177**2.d0)

  eta=eta/(2.d0*13.6058)
  eta_e=eta_e/(2.d0*13.6058)
  w_p_e=w_p_e/(2.d0*13.6058)
  w_av_e=w_av_e/(2.d0*13.6058)
  open(unit=20,file='epsilon.dat',status='unknown')
  open(unit=21,file='absorption.dat',status='unknown')
  eff=0.d0
  eff2=0.d0
  eff_wo=0.d0
  do j=0,nexc-1
     kappa=kmax/dble(nexc)*(dble(j)+0.5d0)
     exc(j+1)=kappa**2.d0/2.d0/ef_mass
     k_values(j+1)=kmax/dble(nexc)*dble(j)
  enddo

!calcolo epsilon_0
  delta_eps=0.d0
  do in=nskip+1,nmodx
     delta_eps=delta_eps+reepsi(in)/freq(in)**2.d0
  enddo
  write(*,*)  'Epsilon_0 : ',delta_eps+ eps_inf




  do i=1,nn
     ene=e_max/dble(nn)*dble(i)/(2.d0*13.6058)
     csca=0.d0
     do in=nskip+1,nmodx
        !csca=csca+reepsi(in)/freq(in)/(freq(in)-ene-(0.d0,1.d0)*eta)
        csca=csca+reepsi(in)/(freq(in)**2.d0-(ene+cmplx(0.d0,eta))**2.d0)
     enddo
     csca=csca+w_p_e**2.d0/(w_av_e**2.d0-(ene+cmplx(0.d0,eta_e))**2.d0)
     csca=csca+1.d0
     enne_cmplx=sqrt(csca)
     enne=dble(enne_cmplx)
     kappa=dimag(enne_cmplx)
     abs=ene*sqrt((-dble(csca)+sqrt(dble(csca*conjg(csca))))/2.d0)
     write(20,*) ene*(2.d0*13.6058)*1000.,real(csca),aimag(csca),-aimag(1.d0/csca)*100.,abs
     !write(20,*) ene*(2.d0*13.6058)*8065.6,real(csca),aimag(csca),-aimag(1.d0/csca),abs 
     write(21,*) ene*(2.d0*13.6058)*8065.6,abs
     do j=1,nexc
        eff(j)=eff(j)+aimag(1.d0/csca)/(ene+exc(j)+offset-(0.d0,1.d0)*eta)
     enddo
  enddo
  do i=1,nn
     ene=e_max/dble(nn)*dble(i)/(2.d0*13.6058)
     csca=0.d0
     do in=nskip+1,nmodx
        csca=csca+reepsi(in)/(freq(in)**2.d0-(ene+cmplx(0.d0,eta))**2.d0)
     enddo
     csca=csca+1.d0
     do j=1,nexc
        eff_wo(j)=eff_wo(j)+aimag(1.d0/csca)/(ene+exc(j)+offset-(0.d0,1.d0)*eta)
     enddo
  enddo
  
  do i=1,nn
     ene=e_max/dble(nn)*dble(i)/(2.d0*13.6058)
     csca=0.d0
     csca=csca+w_p_e**2.d0/(w_av_e**2.d0-(ene+cmplx(0.d0,eta_e))**2.d0)
     csca=csca+1.d0
     do j=1,nexc
        eff2(j)=eff2(j)+aimag(1.d0/csca)/(ene+exc(j)+offset-(0.d0,1.d0)*eta)
     enddo
  enddo

  close(20)
  close(21)


  
 
  

  eff=eff/dble(nn)*e_max/(2.d0*13.6058)
  eff_wo=eff_wo/dble(nn)*e_max/(2.d0*13.6058)
  eff2=eff2/dble(nn)*e_max/(2.d0*13.6058)
  integ=eff*2.d0/3.1415926d0
  eff=eff*1.d0/3.1415926d0!FATTORE 1 DA TEORIA GW
  eff_wo=eff_wo*1.d0/3.1415926d0!FATTORE 1 DA TEORIA GW
  eff2=eff2*1.d0/3.1415926d0!FATTORE 1 DA TEORIA GW

 open(unit=20,file='kappa.dat'//nfile,status='unknown')
  write(20,*) kmax
  write(20,*) nexc
  do j=1,nexc
     write(20,*) k_values(j), dble(eff(j))-dble(eff2(j)),dble(eff(j)),dble(eff2(j)),dble(eff_wo(j))
  enddo
  close(20)

  
  !add complex part for life times 
  do j=1,nexc
     csca=0.d0
     do in=nskip+1,nmodx
          csca=csca+reepsi(in)/(freq(in)**2.d0-(exc(j)+offset+cmplx(0.d0,eta))**2.d0)
     enddo
     csca=csca+1.d0
     eff(j)=eff(j)+(0.,1.d0)*aimag(1.d0/csca)/(ene+exc(j)+offset-(0.d0,1.d0)*eta)
  enddo
  do j=1,nexc
     write(*,*) 'Effective dielectric costant',exc(j)*(2.d0*13.6058),dble(1.d0/eff(j)),aimag(1.d0/eff(j)),integ(j)
  enddo

 ! open(unit=20,file='kappa.dat'//nfile,status='unknown')
 ! write(20,*) kmax
 ! write(20,*) nexc
 ! do j=1,nexc
 !    write(20,*) k_values(j), dble(eff(j))-dble(eff2(j)),dble(eff(j)),dble(eff2(j))
 ! enddo
 ! close(20)


  do j=1,nexc
     write(*,*) exc(j)*(2.d0*13.6058)*1000.d0,dble(1.d0/eff(j))
     !if(dble(1.d0/eff(j))<eps_inf) eff(j)=1.d0/eps_inf
  enddo

  eps_eff=0.d0
  dk=kmax/dble(nexc)
  do j=1,nexc
     kappa=kmax/dble(nexc)*dble(j)
     eps_eff=eps_eff+4.d0*3.1415926d0*kappa**2.d0*dk*dble(eff(j))
     if(mod(j,10)==0) then
        eps_eff_loc=eps_eff+((2*3.1415926d0)**3.d0/omega-4.d0/3.d0*3.14125926d0*kappa**3.d0)/eps_inf
        eps_eff_loc=eps_eff_loc*omega/(2*3.1415926d0)**3.d0
        write(*,*) kappa,  1.d0/eps_eff_loc
     endif
  enddo
  eps_eff=eps_eff+((2*3.1415926d0)**3.d0/omega-4.d0/3.d0*3.14125926d0*kmax**3.d0)/eps_inf

  eps_eff=eps_eff*omega/(2*3.1415926d0)**3.d0

  write(*,*) 'Effective epsilon for GW', 1.d0/eps_eff

  


  deallocate(freq,imepsi,reepsi,oscillator)
  deallocate(eff,exc,ene_values,f_values,k_values)
  stop


end program do_complex_gw

