 subroutine calculate_vg0_ir 
!this subroutine calculate the G=0 element of the Coulomb interatction
!by integrating over q
!and write them on disk

   USE wannier_gw, ONLY : vg_q
   USE wvfct,    ONLY : npw,npwx
   USE gvect
   USE cell_base, ONLY: at, alat, tpiba, omega, tpiba2, bg
   USE constants, ONLY : e2, pi, tpi, fpi
   USE io_global, ONLY : stdout, ionode, ionode_id
   USE io_files,  ONLY : prefix, tmp_dir, diropn
   USE mp_world, ONLY : world_comm, mpime,  nproc
   USE mp, ONLY : mp_bcast

   implicit none

   INTEGER, EXTERNAL :: find_free_unit
   INTEGER :: ig,iun
   INTEGER, PARAMETER :: n_int=20
   INTEGER :: ix,iy,iz,n_int_loc
   REAL(kind=DP) :: qq_fact, qq(3)
   LOGICAL :: exst
   REAL(kind=DP), ALLOCATABLE :: q1(:),q2(:),q3(:)
   REAL(kind=DP) :: qx(3),qy(3),qz(3), qq0,qq1
   REAL(kind=DP) :: k_max,eps_value,qq_mod,qq_delta,rdumm1,rdumm2
   INTEGER :: k_num,ik,qq_num
   REAL(kind=DP), ALLOCATABLE :: k_point(:), k_value(:)

   if(ionode) then
      iun = find_free_unit()
      open(unit=iun,file='kappa.dat',status='old')
      read(iun,*) k_max
      read(iun,*) k_num
   endif
   call mp_bcast(k_max, ionode_id, world_comm)
   call mp_bcast(k_num,ionode_id, world_comm)
   qq_delta=k_max/dble(k_num)
   allocate(k_point(k_num),k_value(k_num))
   if(ionode) then
      do ik=1,k_num
         read(iun,*) k_point(ik),k_value(ik), rdumm1,rdumm2
      enddo
      close(iun)
   endif
   call mp_bcast(k_point, ionode_id, world_comm)
   call mp_bcast(k_value ,ionode_id, world_comm)

   write(stdout,*)'BG1', bg(1:3,1)
   write(stdout,*)'BG2', bg(1:3,2)
    write(stdout,*)'BG3', bg(1:3,3)
    if(.false.) then
   !if(bg(2,1)==0.d0 .and. bg(3,1)==0.d0 .and.bg(1,2)==0.d0 .and.bg(3,2)==0.d0
   !.and. bg(1,3)==0.d0 .and.bg(2,3)==0.d0 ) then
       FLUSH(stdout)
       do ig=1,npw
          vg_q(ig)=0.d0
          if(ig==1 .and. gstart==2) then
             n_int_loc=n_int*50
          else
             n_int_loc=n_int
          endif
          allocate(q1(-n_int_loc+1:n_int_loc))
          allocate(q2(-n_int_loc+1:n_int_loc))
          allocate(q3(-n_int_loc+1:n_int_loc))
          do ix=-n_int_loc+1,n_int_loc                                                        
             q1(ix)=(0.5d0*(1.d0/dble(n_int_loc)*(dble(ix-1))+0.5d0/dble(n_int_loc))*bg(1,1)+g(1,ig))**2.d0 
          enddo
          do ix=-n_int_loc+1,n_int_loc
             q2(ix)=(0.5d0*(1.d0/dble(n_int_loc)*(dble(ix-1))+0.5d0/dble(n_int_loc))*bg(2,2)+g(2,ig))**2.d0
          enddo
          do ix=-n_int_loc+1,n_int_loc
             q3(ix)=(0.5d0*(1.d0/dble(n_int_loc)*(dble(ix-1))+0.5d0/dble(n_int_loc))*bg(3,3)+g(3,ig))**2.d0
          enddo
          do ix=-n_int_loc+1,n_int_loc
             qq0=q1(ix)
             do iy=-n_int_loc+1,n_int_loc
                qq1=qq0+q2(iy)
                do iz=-n_int_loc+1,n_int_loc
                   qq_fact=qq1+q3(iz)
                   qq_mod=dsqrt(q1(ix)+q2(iy)+q3(iz))
                   qq_num=int(qq_mod/qq_delta)
                   if(qq_num>=k_num-2) then
                      write(stdout,*) 'OCIO'
                      eps_value=k_value(k_num)
                   else
                      eps_value=(qq_mod-k_point(qq_num+1))/qq_delta*(k_value(qq_num+2)-k_value(qq_num+1))+k_value(qq_num+1)
                   endif
                   vg_q(ig)=vg_q(ig)+1.d0*eps_value/qq_fact
                enddo
             enddo
          enddo
          vg_q(ig)=vg_q(ig)*e2*fpi/(8.d0*(dble(n_int_loc))**3.d0)/tpiba2
          deallocate(q1,q2,q3)
       enddo
    else
       do ig=1,npw
          vg_q(ig)=0.d0
          if(ig==1 .and. gstart==2) then
             n_int_loc=n_int!*50
          else
             n_int_loc=n_int
          endif
         

          do ix=-n_int_loc+1,n_int_loc
             do iy=-n_int_loc+1,n_int_loc
                do iz=-n_int_loc+1,n_int_loc
                   
                   qx(:)=0.5d0*(1.d0/dble(n_int_loc)*(dble(ix-1))+0.5d0/dble(n_int_loc))*bg(:,1)
                   qy(:)=0.5d0*(1.d0/dble(n_int_loc)*(dble(iy-1))+0.5d0/dble(n_int_loc))*bg(:,2)
                   qz(:)=0.5d0*(1.d0/dble(n_int_loc)*(dble(iz-1))+0.5d0/dble(n_int_loc))*bg(:,3)
                   
                   qq(1:3)=qx(1:3)+qy(1:3)+qz(1:3)
                   qq_mod=dsqrt(qq(1)**2.d0+qq(2)**2.d0+qq(3)**2.d0)*tpiba
                   qq_num=int(qq_mod/qq_delta)
                   if(qq_num>=k_num-2) then
                     write(stdout,*) 'OCIO'                                                                                                                                                  
                      eps_value=1.d0!k_value(k_num)
                   else
                      eps_value=(qq_mod-k_point(qq_num+1))/qq_delta*(k_value(qq_num+2)-k_value(qq_num+1))+k_value(qq_num+1)
                   endif

                  
                   qq(:)=qx(:)+qy(:)+qz(:)+g(:,ig)
                   qq_fact=qq(1)**2+qq(2)**2+qq(3)**2
                   
                   if(ig==1.and. gstart==2) then
                      vg_q(1)=vg_q(ig)+1.d0*eps_value/qq_fact
                   else
                      
                      
                       qq_fact=qq(1)**2+qq(2)**2+qq(3)**2
                       vg_q(ig)=vg_q(ig)+1.d0*eps_value/qq_fact
                                             
                   endif


                   
                enddo
             enddo
          enddo
          vg_q(ig)=vg_q(ig)*e2*fpi/(8.d0*(dble(n_int_loc))**3.d0)/tpiba2
       enddo
    endif
   vg_q(:)=vg_q(:)/omega
   if(gstart==2) write(stdout,*) 'V(G=0) = ',vg_q(1) 
!   if(ionode) then
!      iun = find_free_unit()
!      open( unit= iun, file=trim(tmp_dir)//trim(prefix)//'.vg_q',
!      status='unknown',form='unformatted')
!      write(iun) vg_q(1:npw)
!      close(iun)
!   endif

   iun = find_free_unit()
   CALL diropn( iun, 'vgq', npwx, exst )
   CALL davcio(vg_q,npwx,iun,1,1)
   close(iun)
   deallocate(k_point,k_value)


   return
 end subroutine calculate_vg0_ir

subroutine  exchange_full_ir_k

  USE io_files,             ONLY : prefix, iunwfc, nwordwfc
  USE kinds,    ONLY : DP
  USE wavefunctions, ONLY : evc,psic
  USE wvfct,                ONLY : nbnd,npw,npwx
  USE gvect, ONLY : ngm, gstart,gg, g
  USE cell_base, ONLY : omega, tpiba2, bg,tpiba
  USE constants, ONLY : e2, pi, tpi, fpi,rytoev
  USE mp, ONLY : mp_sum, mp_barrier
  USE io_global, ONLY : stdout,ionode
  USE fft_base,         ONLY : dffts,dfftp
  USE fft_interfaces,ONLY : fwfft, invfft
  USE klist,                ONLY : nks,ngk,xk
  USE lsda_mod,   ONLY : lsda, nspin,current_spin,isk
  USE becmod,        ONLY : bec_type, becp, calbec,allocate_bec_type, deallocate_bec_type
  USE uspp,     ONLY : nkb, vkb, becsum, nhtol, nhtoj, indv, okvan
  USE uspp_param, ONLY : upf, nh
  USE ions_base,  ONLY : nat, nsp, ityp
  USE spin_orb, ONLY: lspinorb
  USE noncollin_module, ONLY: npol, noncolin
  USE wannier_gw, ONLY : l_truncated_coulomb, truncation_radius,num_nbndv,num_nbnds,&
                             &s_first_state,s_last_state
  USE mp_world, ONLY : world_comm, mpime,  nproc
  USE klist,    ONLY : igk_k

  implicit none
!first k point MUST be Gamma (at the moment)

  COMPLEX(kind=DP), ALLOCATABLE :: rwfc1(:,:),rwfc2(:,:),rprod(:),prod(:)
  INTEGER ii,ipol,ik,jj,ig
  REAL(kind=DP) :: dk(3)
  REAL(kind=DP), ALLOCATABLE :: fac(:),sigma(:)
  REAL(kind=DP) :: sca

  allocate(rwfc1(dfftp%nnr*npol,nbnd))
  allocate(rwfc2(dfftp%nnr*npol,nbnd))
  allocate(fac(ngm))
  allocate(rprod(dfftp%nnr*npol))
  allocate(prod(ngm*npol),sigma(nbnd))

  call davcio (evc, 2*nwordwfc, iunwfc, 1, -1)

!put it on real space
  npw = ngk (1)
  rwfc1=(0.d0,0.d0)
  do ii=1,nbnd
     do ipol=0,npol-1
        psic(1:dffts%nnr)=0.d0
        psic(dffts%nl(igk_k(1:npw,1)))=evc(1+ipol*npwx:ipol*npwx+npw,ii)
        CALL invfft ('Wave', psic, dffts)
        rwfc1(1+ipol*dffts%nnr:ipol*dffts%nnr+dffts%nnr,ii)=psic(1:dffts%nnr)
     enddo
   enddo
   sigma=0.d0
   
   do ik=1,nks
      npw = ngk (ik)
      dk(1:3)=xk(1:3,ik)-xk(1:3,1)
      !write(stdout,*) 'CALL  v_product_ir_k'
      !call v_product_ir_k(dk,fac)
      !write(stdout,*) 'DONE  v_product_ir_k'
      call davcio (evc, 2*nwordwfc, iunwfc, ik, -1)
      do ii=1,nbnd
         do ipol=0,npol-1
            psic(1:dffts%nnr)=0.d0
            psic(dffts%nl(igk_k(1:npw,ik)))=evc(1+ipol*npwx:ipol*npwx+npw,ii)
            CALL invfft ('Wave', psic, dffts)
            rwfc2(1+ipol*dffts%nnr:ipol*dffts%nnr+dffts%nnr,ii)=psic(1:dffts%nnr)
         enddo
      enddo
      do jj=s_first_state,s_last_state
         write(stdout,*) 'CALL  v_product_ir_k',ik,jj
         call v_product_ir_k(dk,fac,jj)
         write(stdout,*) 'DONE  v_product_ir_k'

         do ii=s_first_state,s_last_state
            rprod(1:dffts%nnr*npol)=conjg(rwfc2(1:dffts%nnr*npol,jj))*rwfc1(1:dffts%nnr*npol,ii)
            prod=0.d0
            do ipol=0,npol-1
               psic=0.d0
               psic(1:dffts%nnr)=rprod(1+dffts%nnr*ipol:dffts%nnr*ipol+dffts%nnr)
               CALL fwfft ('Rho', psic, dfftp)
               do ig=1,ngm
                  prod(ig+ngm*ipol)=psic(dfftp%nl(ig))
               enddo
            enddo
            sca=0.d0
            do ipol=0,npol-1
               do ig=1,ngm
                  sca=sca+fac(ig)*conjg(prod(ig+ngm*ipol))*prod(ig+ngm*ipol)
               enddo
            enddo
            call mp_sum(sca,world_comm)
            sigma(ii)=sigma(ii)+sca
         enddo
      enddo

   enddo
   do ii=s_first_state,s_last_state
      write(stdout,*) 'SIGMA IR',ii, sigma(ii)*rytoev
   enddo
  deallocate(rwfc1,rwfc2,fac)
  deallocate(prod,rprod,sigma)

  return

end subroutine exchange_full_ir_k


subroutine v_product_ir_k(dk,fac, iband)
  USE kinds, ONLY : DP
  USE wvfct, ONLY : npw,npwx,et
  USE mp, ONLY : mp_sum,mp_barrier,mp_bcast
  USE klist, ONLY : nks,ngk,xk
  USE noncollin_module, ONLY: npol, noncolin
  USE mp_world, ONLY : world_comm
  USE spin_orb, ONLY: lspinorb
  USE io_global, ONLY : stdout, ionode, ionode_id
  USE gvect, ONLY : ngm, gstart,gg, g
  USE constants, ONLY : e2, fpi
  USE cell_base, ONLY: tpiba,tpiba2,omega,bg,at
  USE io_files,  ONLY : prefix, tmp_dir
  USE klist, ONLY : nks,xk
  USE fft_base,             ONLY : dfftp, dffts
  USE fft_interfaces,       ONLY : fwfft, invfft
  USE io_files, ONLY : prefix, tmp_dir, diropn
  USE wavefunctions, ONLY : psic
  
  implicit none
 

  REAL(kind=DP) :: dk(3)!k point in units of tpiba
  REAL(kind=DP) :: fac(ngm)
  INTEGER :: iband!band file

  INTEGER :: nkpoints
  INTEGER, EXTERNAL :: find_free_unit
   INTEGER :: ig,iun
   INTEGER, PARAMETER :: n_int=20
   INTEGER :: ix,iy,iz,n_int_loc
   REAL(kind=DP) :: qq_fact, qq(3)
   LOGICAL :: exst
   REAL(kind=DP), ALLOCATABLE :: q1(:),q2(:),q3(:)
   REAL(kind=DP) :: qx(3),qy(3),qz(3), qq0,qq1
   REAL(kind=DP) :: k_max,eps_value,qq_mod,qq_delta
   INTEGER :: k_num,ik,qq_num
   REAL(kind=DP), ALLOCATABLE :: k_point(:), k_value(:)
   CHARACTER(4) :: nfile

   write(nfile,'(4i1)') iband/1000,mod(iband,1000)/100,mod(iband,100)/10,mod(iband,10)


!check total number of k-points
  nkpoints=nks**(1.d0/3.d0)
  if(nkpoints**3 < nks) then
     nkpoints=nkpoints+1
  else if (nkpoints**3 > nks) then
     nkpoints=nkpoints-1
  endif
  if  (nkpoints**3 /= nks) then
     write(stdout,*) 'Problem with k-points mesh', nkpoints, nks
     stop
  endif
  write(stdout,*) 'DEBUG', dk,nkpoints
  if(ionode) then
      iun = find_free_unit()
      open(unit=iun,file='kappa.dat'//nfile,status='old')
      read(iun,*) k_max
      read(iun,*) k_num
   endif
   call mp_bcast(k_max, ionode_id, world_comm)
   call mp_bcast(k_num,ionode_id, world_comm)
   qq_delta=k_max/dble(k_num)
   allocate(k_point(k_num),k_value(k_num))
   if(ionode) then
      do ik=1,k_num
         read(iun,*) k_point(ik),k_value(ik)
      enddo
      close(iun)
   endif
   call mp_bcast(k_point, ionode_id, world_comm)
   call mp_bcast(k_value ,ionode_id, world_comm)

   !do ig=1,ngm
   fac=0.d0
   !if(ig==1 .and. gstart==2 .and. dk(1)==0.d0 .and. dk(2)==0.d0 .and. dk(3)==0.d0) then
   !      n_int_loc=n_int!*50                                                                                               
   !   else
   n_int_loc=n_int
   !   endif


   do ix=-n_int_loc+1,n_int_loc
      do iy=-n_int_loc+1,n_int_loc
         do iz=-n_int_loc+1,n_int_loc
               
            qx(:)=0.5d0*(1.d0/dble(n_int_loc*nkpoints)*(dble(ix-1))+0.5d0/dble(n_int_loc*nkpoints))*bg(:,1)
            qy(:)=0.5d0*(1.d0/dble(n_int_loc*nkpoints)*(dble(iy-1))+0.5d0/dble(n_int_loc*nkpoints))*bg(:,2)
            qz(:)=0.5d0*(1.d0/dble(n_int_loc*nkpoints)*(dble(iz-1))+0.5d0/dble(n_int_loc*nkpoints))*bg(:,3)
            
            qq(1:3)=qx(1:3)+qy(1:3)+qz(1:3)+dk(1:3)
            qq_mod=dsqrt(qq(1)**2.d0+qq(2)**2.d0+qq(3)**2.d0)*tpiba
            qq_num=int(qq_mod/qq_delta)
            if(qq_num>=k_num-2) then
               eps_value=k_value(k_num)                                                                            
            else
               eps_value=(qq_mod-k_point(qq_num+1))/qq_delta*(k_value(qq_num+2)-k_value(qq_num+1))+k_value(qq_num+1)
            endif

           
            do ig=1,ngm
               qq(:)=qx(:)+qy(:)+qz(:)+g(:,ig)+dk(:)
               qq_fact=qq(1)**2+qq(2)**2+qq(3)**2

               fac(ig)=fac(ig)+eps_value/qq_fact
            enddo



         enddo
      enddo
   enddo
   fac=fac*e2*fpi/(8.d0*(dble(n_int_loc))**3.d0)/tpiba2
   !enddo

   fac=fac/omega/dble(nks)
   if(gstart==2) write(stdout,*) 'FAC(G=0) = ',fac(1)
   deallocate(k_point,k_value)
  return
end subroutine v_product_ir_k
